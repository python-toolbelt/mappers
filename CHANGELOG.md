# Changelog

Tracking changes in `mappers` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/mappers/-/releases

## next release
