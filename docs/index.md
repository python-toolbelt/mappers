# mappers

*Library built on top of `fun-composer` with a plenty of useful transformations built-in.*

It's currently used as the data mapping backend in [`pyroman` ORM](https://gitlab.com/pyroman-orm/pyroman-orm).

For documentation see tests: [https://gitlab.com/python-toolbelt/mappers/-/tree/main/tests](https://gitlab.com/python-toolbelt/mappers/-/tree/main/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/python-toolbelt/mappers/-/issues](https://gitlab.com/python-toolbelt/mappers/-/issues)

## Requirements

Install requirements for setup beforehand using

```bash
poetry install -E test
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## See Also

This library was inspired by:

* Ruby's library called [transproc](https://github.com/solnic/transproc)

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/python-toolbelt/mappers/-/blob/main/LICENSE) for more information.
