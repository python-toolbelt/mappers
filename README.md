# mappers
[![pipeline status](https://gitlab.com/python-toolbelt/mappers/badges/main/pipeline.svg)](https://gitlab.com/python-toolbelt/mappers/-/commits/main)
[![coverage report](https://gitlab.com/python-toolbelt/mappers/badges/main/coverage.svg)](https://gitlab.com/python-toolbelt/mappers/-/commits/main)

*Library built on top of `fun-composer` with a plenty of useful transformations built-in.*

**This is the readme for developers.** The documentation for users is available here: [https://python-toolbelt.gitlab.io/mappers](https://python-toolbelt.gitlab.io/mappers)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/python-toolbelt/mappers/-/issues](https://gitlab.com/python-toolbelt/mappers/-/issues)

## Requirements for builds

Install requirements for setup beforehand using

```bash
poetry install -E test -E build
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/python-toolbelt/mappers/-/blob/main/LICENSE) for more information.
